/**
 * Created by jhacking on 5/8/2017.
 */
//ES6 service, working also with promise for async to work.

import 'whatwg-fetch';

// ES6 Function
class HttpService {
    getProducts = () => {
        var promise = new Promise((resolve, reject) => {
            fetch('http://localhost:3004/api/product').then(response => {
                resolve(response.json());
            })
        });

        return promise;

    }
}

export default HttpService;